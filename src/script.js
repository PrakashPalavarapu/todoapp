let inputDiv = document.querySelector(".inputDiv");
let textInput = document.querySelector("input[type=text]");
let body = document.body;

// main Input Border Decoration
document.addEventListener("click", () => {
  inputDiv.style.borderColor = "rgb(152,152,152) border:opacity-0.5";
});

textInput.addEventListener("click", function (event) {
  inputDiv.style.border = "1px rgb(184,63,69) solid";
  event.stopPropagation();
});

let listsDiv = document.querySelector(".listsDiv");

// Create Lits Items and styling

function creatListItem(text) {
  let itemDiv = document.createElement("div");
  itemDiv.setAttribute("name", "items");
  itemDiv.className =
    "itemDiv flex justify-end items-center w-[500px] mx-auto relative border-[1px] border-solid p-3 border-r-[1px] border-b-[1px] border-l-[1px]";
  itemDiv.dataset.isActive = true;

  let itemCheckBox = document.createElement("input");
  itemCheckBox.setAttribute("name", "itemCheckbox");
  itemCheckBox.type = "checkbox";
  itemCheckBox.classList += "absolute start-0";

  let itemText = document.createElement("input");
  itemText.setAttribute("name", "itemText");
  itemText.type = "text";
  itemText.value = text;
  itemText.readOnly = true;
  itemText.classList =
    "absolute text-[20px] w-[80%] font-Helvetica start-[40px] outline-none";

  let crossButton = document.createElement("button");
  crossButton.setAttribute("name", "crossButton");
  crossButton.textContent = "X";
  crossButton.classList += "text-[rgb(184,63,69)] opacity-0" 

  listsDiv.appendChild(itemDiv);
  itemDiv.appendChild(itemCheckBox);
  itemDiv.appendChild(itemText);
  itemDiv.appendChild(crossButton);
}

let optionsDiv = document.querySelector(".optionsDiv");
optionsDiv.classList.add("hidden");

let maincheck = document.querySelector(".globalCheckBox");

function appendListItems() {
  textInput.addEventListener("keypress", function (e) {
    if (e.key === "Enter") {
      if (textInput.value.length > 1) {
        creatListItem(textInput.value);
        textInput.value = "";
        optionsDiv.classList.remove("hidden");
        // maincheck.classList.remove("hidden");
        document.querySelector(".mainCheckBoxLabel").classList.remove("hidden");
        inputDiv.classList.remove("pl-[50px]");
        inputDiv.classList.add("pl-[5px]");
        maincheck.classList.add("mr-[30px]");
        inputDiv.classList.remove("shadow-2xl");
      }
      updateActiveCount();
    }
  });
}
appendListItems();

function editListItems() {
  document.addEventListener("dblclick", (event) => {
    if (event.target.name === "itemText") {
      event.target.readOnly = "";
      event.target.previousSibling.style.display = "none";
      event.target.nextSibling.style.display = "none";
      event.target.classList =
        "text-[20px] w-[100%] font-Helvetica outline-none";
    }
  });

  document.addEventListener("keypress", (event) => {
    if (event.target.name === "itemText") {
      if (event.target.value.length > 1) {
        // let docClick = false;
        if (event.key === "Enter") {
          event.target.previousSibling.style.display = "";
          event.target.nextSibling.style.display = "";
          event.target.classList =
            "absolute text-[20px] w-[80%] font-Helvetica start-[40px] outline-none";
          event.target.readOnly = true;
        }
      }
    }
  });
}
editListItems();

function removeListItem() {
  document.addEventListener("click", (event) => {
    if (event.target.name == "crossButton") {
      event.target.parentNode.remove();
    }
    let activeCount = updateActiveCount();
    if(listsDiv.childElementCount == 0){
      optionsDiv.classList.add("hidden");
      document.querySelector(".mainCheckBoxLabel").classList.add('hidden')
    }
  });
}
removeListItem();

// oncheck text chagne and on un-check remove strike through

function onCheckStrikeThrough() {
  document.addEventListener("click", (event) => {
    if (event.target.name == "itemCheckbox") {
      if (event.target.checked) {
        event.target.parentNode
          .querySelector(`:nth-child(2)`)
          .classList.add("line-through");
        event.target.parentNode.dataset.isActive = false;
        console.log("status", event.target.parentNode.dataset.isActive);
        updateActiveCount();
      } else {
        event.target.parentNode
          .querySelector(`:nth-child(2)`)
          .classList.remove("line-through");
        event.target.parentNode.dataset.isActive = true;
        console.log("status", event.target.parentNode.dataset.isActive);
        updateActiveCount();
      }
    }
  });
}
onCheckStrikeThrough();

// checkAllItems function

function checkAllItems() {
  document.addEventListener("click", (event) => {
    if (event.target.classList.contains("globalCheckBox")) {
      let checkBoxes = document.querySelectorAll("input[type=checkbox]");
      if (event.target.checked) {
        for (let item of checkBoxes) {
          if (item.name === "itemCheckbox") {
            item.checked = true;
            item.nextElementSibling.classList.add("line-through");
            item.parentNode.dataset.isActive = false;
            // console.log(item.parentNode.dataset.isActive);
          }
        }
        updateActiveCount();
      } else {
        for (let item of checkBoxes) {
          if (item.name === "itemCheckbox") {
            item.checked = false;
            item.nextElementSibling.classList.remove("line-through");
            item.parentNode.dataset.isActive = true;
            // console.log(item.parentNode.dataset.isActive);
          }
        }
        updateActiveCount();
      }
    }
  });
}
checkAllItems();

// active flag for each item
function updateActiveCount() {
  let allItems = listsDiv.children;
  let activeCount = 0;
  for (listItem of allItems) {
    if (listItem.dataset.isActive == "true") {
      activeCount += 1;
    }
  }
  document.querySelector(
    ".activeStatus"
  ).textContent = `${activeCount} items left!`;

  return activeCount;
}

// Active Button

function showAllActive() {
  let allButton = document.querySelector(".showAllActive");
  document.addEventListener("click", function (event) {
    if (event.target == allButton) {
      let lists = listsDiv.children;
      for (item of lists) {
        if (item.dataset.isActive != "true") {
          item.classList.add("hidden");
        } else {
          item.classList.remove("hidden");
        }
      }
    }
  });
}

showAllActive();

function showAllCompleted() {
  let allButton = document.querySelector(".showAllCompleted");
  allButton.addEventListener("click", function () {
    let lists = listsDiv.children;
    for (item of lists) {
      if (item.dataset.isActive == "true") {
        item.classList.add("hidden");
      } else {
        item.classList.remove("hidden");
      }
    }
  });
}
showAllCompleted();

function showAll() {
  let allButton = document.querySelector(".showAllItems");
  allButton.addEventListener("click", function () {
    let lists = listsDiv.children;
    for (item of lists) {
      item.classList.remove("hidden");
    }
  });
}
showAll();

// clear completed
function clearCompleted() {
  let clearCompletedButton = document.querySelector(".clearCompleted");
  clearCompletedButton.addEventListener("click", function () {
    let lists = listsDiv.children;
    let notDelete = [];
    for (let index = 0; index < lists.length; index++) {
      if (lists[index].dataset.isActive == "true") {
        notDelete.push(lists[index]);
        console.log(lists[index]);
      }
    }
    listsDiv.innerHTML = "";
    for (items of notDelete) {
      listsDiv.appendChild(items);
    }
  });
}
clearCompleted();

// hover styles for crossbutton
document.addEventListener('mouseover',(event)=>{
  if(event.target.name == "itemText" || event.target.name == "crossButton"){
    event.target.parentNode.lastElementChild.classList.remove('opacity-0')
    event.target.parentNode.lastElementChild.classList.add('opacity-1')
  }
});

document.addEventListener('mouseout',(event)=>{
  if(event.target.name == "itemText" || event.target.name == "crossButton"){
    event.target.parentNode.lastElementChild.classList.remove('opacity-1')
    event.target.parentNode.lastElementChild.classList.add('opacity-0')
  }
});